const axios = require('axios')
const aladhan = require('./src/data-config/aladhan')

module.exports = function (api) {
  api.loadSource(async actions => {
    // Al-Adhan API for fetch Pray times & Ramadhan counter down
    const { data } = await axios.get(aladhan.JAKARTA_PRAY_TIMES)

    const prayTimesCollection = actions.addCollection('PrayTimesJaksel')

    let prayTimeDate = 1;
    for (const prayTime of data.data) {
      prayTimesCollection.addNode({
        id: prayTimeDate,
        date: prayTime.date,
        timings: prayTime.timings,
        hijriDate: prayTime.hijri,
      })
      prayTimeDate++;
    }
  })

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  })
}
