const tailwind = require('tailwindcss')
const purgecss = require('@fullhuman/postcss-purgecss')

const postCssPlugins = [
  tailwind(),
]

if (process.env.NODE_ENV === 'production') postcssPlugins.push(purgecss(require('./purgecss.config.js')))
module.exports = {
  siteName: 'The MumMi JS',
  plugins: [],
  css: {
    loaderOptions: {
      postcss: {
        plugins: postCssPlugins,
      }
    }
  }
}
