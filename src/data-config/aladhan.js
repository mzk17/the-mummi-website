/** 
 * Example
 * 'http://api.aladhan.com/v1/calendarByCity?city=Jakarta%20Selatan&country=Indonesia&method=1&month=03&year=2020&methodSettings=20.0,null,18.0&tune=0,-5,0,4,3,0,3,3,0'
*/

const BASE_URL_V1 = 'http://api.aladhan.com/v1/'
const PRAY_TIMES = {
    jakartaSelatan: {
        calendarByCity: 'calendarByCity?',
        city: 'Jakarta%20Selatan',
        country: 'Indonesia',
        method: '1',
        month: new Date().getMonth() + 1,
        year: new Date().getFullYear(),
        methodSettings: '20.0,null,18.0',
        tune: '0,-5,0,4,3,0,3,3,0'
    }
}
const JAKARTA_PRAY_TIMES = BASE_URL_V1 + PRAY_TIMES.jakartaSelatan.calendarByCity + `${[
    `city=${PRAY_TIMES.jakartaSelatan.city}`,
    `country=${PRAY_TIMES.jakartaSelatan.country}`,
    `method=${PRAY_TIMES.jakartaSelatan.method}`,
    `month=${PRAY_TIMES.jakartaSelatan.month}`,
    `year=${PRAY_TIMES.jakartaSelatan.year}`,
    `methodSettings=${PRAY_TIMES.jakartaSelatan.methodSettings}`,
    `tune=${PRAY_TIMES.jakartaSelatan.tune}`
].join('&')}`

module.exports = {
    JAKARTA_PRAY_TIMES
}